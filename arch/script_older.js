function onLoad(){
//	document.addEventListener("deviceready", onDeviceReady, true);
}


function onDeviceReady(){
	window.plugins.speechrecognizer.init(speechInitOk, speechInitFail);
}

function speechInitOk() {
//        alert("we are good");
//        supportedLanguages();
        recognizeSpeech();
    }
    function speechInitFail(m) {
        alert(m);
    }

    // Show the list of the supported languages
    function supportedLanguages() {
        window.plugins.speechrecognizer.getSupportedLanguages(function(languages){
                // display the json array
                alert(languages);
            }, function(error){
                alert("Could not retrieve the supported languages");
        });
    }

    function recognizeSpeech() {
        var requestCode = 1234;
        var maxMatches = 5;
        var promptString = "Please say a command";  // optional
        var language = "en-US";                     // optional
        window.plugins.speechrecognizer.startRecognize(speechOk, speechFail, requestCode, maxMatches, promptString, language);
    }

    function speechOk(result) {
        var respObj, requestCode, matches;
        if (result) {
            respObj = JSON.parse(result);
            if (respObj) {
                var matches = respObj.speechMatches.speechMatch;

                for (x in matches) {
                    alert("possible match: " + matches[x]);
                    // regex comes in handy for dealing with these match strings
                }
            }        
        }
    }

    function speechFail(message) {
        console.log("speechFail: " + message);
    }

// JQuwery

$(function(){
	onLoad();
	if (~window.location.search.indexOf('touch')) {
		Modernizr.touch = true;
	}
	
	if (Modernizr.touch) {
		$(document).ready(function() {
			removeUrlBar();
		});
		
		var iPhone = navigator.userAgent.match(/iPhone/i);
		var iPad = navigator.userAgent.match(/iPad/i);
		if (iPhone || iPad) {
			document.body.addEventListener('touchmove', function(e) {
				e.preventDefault();
			});
			
			$(document).ready(function() {
				$('.scrollable').each(function() {
					$(this).addClass('shadow');
					new Scroller(this);
				});
			});
		}
	}
	
//	testLawn();

});

function removeUrlBar() {
	var height = $('body').height();
	var iPhone = navigator.userAgent.match(/iPhone/i);
	var iPad = navigator.userAgent.match(/iPad/i); 

	if (iPhone || iPad) {
		var footer = 16;
		$('#footer').css({
			top: height + footer
		});
		$('#map-wrapper').css({
			bottom: -(footer),
			height: 'auto'
		});
		$('.scroll-frame').css({
			bottom: -footer
		});
	} else {
		$('#header').css({
			position: 'fixed',
			top: 0,
			zIndex: 100
		});

		$('#footer').css({
			position: 'fixed',
			bottom: 0
		});

		$('#map-wrapper').css({
			top: 33,
			bottom: 43,
			height: 'auto'
		});

		$('.scroll-frame').css({
			height: 'auto',
			top: 33,
			paddingBottom: 43,
			bottom: 'auto'
		});
	}

	window.addEventListener('load', function() {
		window.setTimeout(function() {
			window.scrollTo(0, 1);
		}, 0);
	});
};

function testLawn(){
	var me = {name:'brian'};
	var me2 = {name:'marcin'};
	var rec = Lawnchair({
		name:'people', 
		record:'person',
		adapter: 'webkit-sqlite'
	}, function(people){

		});
	rec.nuke();
	rec.save({key:"1",value:me});
	rec.save({key:"2",value:me2});
	
	// wydobycie wartości
	setTimeout(function(){
		rec.get("1",function(obj){
//			console.log(obj);
		});	
	},300);
	
	// modyfikacja
	setTimeout(function(){
			rec.get("1",function(thisobj){
//			console.log(thisobj);
			var obj = {};
				obj = thisobj.value;
				obj.name = "Zmodyfikowany";
			rec.save({key:thisobj.key,value:obj});
		});
	},500);
	
	
	// iteracja przez wszystkie
	setTimeout(function(){
		var arr = [];
		rec.all(function(arrBeers){
			for(var i = 0; i<arrBeers.length;i++)
			{
				var el = arrBeers[i].value.name;
//				console.log(el);
				arr.push(el);
			}
			alert(arr);
		});
	},800);
	
	
}