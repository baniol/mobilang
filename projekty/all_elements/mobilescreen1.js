/*
 * JS for mobilescreen1 generated by Exadel Tiggzi
 *
 * Created on: Sunday, September 09, 2012, 08:05:18 AM (PDT)
 */
/************************************
 * JS API provided by Exadel Tiggzi  *
 ************************************/
/* Setting project environment indicator */
Tiggr.env = "bundle";

function navigateTo(outcome, useAjax) {
    Tiggr.navigateTo(outcome, useAjax);
}

function adjustContentHeight() {
    Tiggr.adjustContentHeight();
}

function adjustContentHeightWithPadding() {
    Tiggr.adjustContentHeightWithPadding();
}

function setDetailContent(pageUrl) {
    Tiggr.setDetailContent(pageUrl);
}
/**********************
 * SECURITY CONTEXTS  *
 **********************/
/*******************************
 *      SERVICE SETTINGS        *
 ********************************/
/*************************
 *      SERVICES          *
 *************************/
createSpinner("files/resources/lib/jquerymobile/images/ajax-loader.gif");
Tiggr.AppPages = [{
    "name": "mobilescreen1",
    "location": "mobilescreen1.html"
}];
j_0_js = function(runBeforeShow) { /* Object & array with components "name-to-id" mapping */
    var n2id_buf = {
        'mobilelabel1': 'j_4',
        'mobileradiogroup1': 'j_5',
        'mobileradiobutton1': 'j_6',
        'mobileradiobutton2': 'j_7',
        'mobileradiobutton3': 'j_8',
        'mobiletextarea1': 'j_9',
        'mobilebutton1': 'j_10',
        'mobiletextinput1': 'j_11',
        'mobilelink1': 'j_12',
        'mobiledatepicker1': 'j_13',
        'mobilecheckboxgroup2': 'j_14',
        'mobilecheckbox4': 'j_15',
        'mobilecheckbox5': 'j_16',
        'mobilecheckbox6': 'j_17',
        'mobileslider1': 'j_18',
        'mobileselectmenu1': 'j_19',
        'selectmenuitem1': 'j_20',
        'mobilelist2': 'j_21',
        'mobilelistitem4': 'j_22',
        'mobilelistitem5': 'j_23',
        'mobilelistitem6': 'j_24',
        'mobilecollapsblock3': 'j_25',
        'mobilecollapsblockheader3': 'j_26',
        'mobilecollapsblockcontent3': 'j_27',
        'mobilespacer1': 'j_28',
        'mobilecarousel1': 'j_29',
        'mobilecarouselitem1': 'j_30',
        'mobilecarouselitem2': 'j_31',
        'mobilecarouselitem3': 'j_32',
        'mobilenavbar1': 'j_34',
        'mobilenavbaritem1': 'j_35',
        'mobilenavbaritem2': 'j_36',
        'mobilenavbaritem3': 'j_37'
    };
    if ("n2id" in window && window.n2id !== undefined) {
        $.extend(n2id, n2id_buf);
    } else {
        window.n2id = n2id_buf;
    }
    Tiggr.CurrentScreen = 'j_0';
    /*************************
     * NONVISUAL COMPONENTS  *
     *************************/
    var datasources = [];
    /************************
     * EVENTS AND HANDLERS  *
     ************************/
    j_0_beforeshow = function() {
        Tiggr.CurrentScreen = 'j_0';
        for (var idx = 0; idx < datasources.length; idx++) {
            datasources[idx].__setupDisplay();
        }
    }
    // screen onload
    screen_561A_onLoad = j_0_onLoad = function() {
        screen_561A_elementsExtraJS();
        j_0_windowEvents();
        screen_561A_elementsEvents();
    }
    // screen window events
    screen_561A_windowEvents = j_0_windowEvents = function() {
        $('#j_0').bind('pageshow orientationchange', function() {
            adjustContentHeightWithPadding();
        });
    }
    // screen elements extra js
    screen_561A_elementsExtraJS = j_0_elementsExtraJS = function() {
        // screen (screen-561A) extra code
        //DatePicker component JS
        mobiledatepicker1_selector = "#j_13";
        mobiledatepicker1_dataPickerOptions = {
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            showOtherMonths: true
        };
        Tiggr.__registerComponent('mobiledatepicker1', new Tiggr.TiggrMobileDatePickerComponent("j_13", mobiledatepicker1_dataPickerOptions));
        $("#j_19").parent().find("a.ui-btn").attr("tabindex", "9");
        listView = $("#j_21");
        theme = listView.attr("data-theme");
        if (typeof theme !== 'undefined') {
            var themeClass = "ui-btn-up-" + theme;
            listItem = $("#j_21 .ui-li-static");
            $.each(listItem, function(index, value) {
                $(this).addClass(themeClass);
            });
        }
        Tiggr.__registerComponent('mobilecarousel1', new Tiggr.TiggrMobileCarouselComponent("j_29"));
        $("#j_30").attr("reRender", "mobilecarousel1");
        $("#j_31").attr("reRender", "mobilecarousel1");
        $("#j_32").attr("reRender", "mobilecarousel1");
    }
    // screen elements handler
    screen_561A_elementsEvents = j_0_elementsEvents = function() {
        $("a :input,a a,a fieldset label").live({
            click: function(event) {
                event.stopPropagation();
            }
        });
    }
    $("#j_0").die("pagebeforeshow").live("pagebeforeshow", function(event, ui) {
        j_0_beforeshow();
    });
    if (runBeforeShow) {
        j_0_beforeshow();
    } else {
        j_0_onLoad();
    }
}
$("#j_0").die("pageinit").live("pageinit", function(event, ui) {
    j_0_js();
});