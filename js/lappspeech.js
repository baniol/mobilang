var LappSpeech = {
	
	langSwitch:"en-US",
	
	initSpeech: function(){
		var self = this;
		$(document).on(this.startEvent,'#start-speech',function(){
			LappSpeech.langSwitch = "en-US"
			window.plugins.speechrecognizer.init(self.recognizeSpeech, self.speechInitFail);
		});
		
		$(document).on(this.startEvent,'#start-speech-pl',function(){
			LappSpeech.langSwitch = "pl-PL"
			window.plugins.speechrecognizer.init(self.recognizeSpeech, self.speechInitFail);
		});
	},
	
//	onDeviceReady:function(){
//		window.plugins.speechrecognizer.init(self.speechInitOk, self.speechInitFail);
//	},
	
	recognizeSpeech: function() {
		var self = this;
        var requestCode = 1234;
//        var maxMatches = 1;
        var promptString = "Zacznij mówić";  // optional
//        var language = LappSpeech.langSwitch;                     // optional
//		alert(LappSpeech.langSwitch);
        window.plugins.speechrecognizer.startRecognize(LappSpeech.speechOk, LappSpeech.speechFail, requestCode, LappSpeech.langSwitch, promptString);
	},
	
	speechFail: function(message) {
        alert("speechFail: " + message);
    },
	
	speechOk: function(result) {
		var self = this;
        var respObj, requestCode, matches;
        if (result) {
            respObj = JSON.parse(result);
            if (respObj) {
                var matches = respObj.speechMatches.speechMatch;

                for (x in matches) {
//                    alert(matches[x]);
                    LappSpeech.insertResult(matches[x]);
                    // regex comes in handy for dealing with these match strings
                }
            }        
        }
    },
	
//	supportedLanguages: function() {
//        window.plugins.speechrecognizer.getSupportedLanguages(function(languages){
//                // display the json array
//                alert(languages);
//            }, function(error){
//                alert("Could not retrieve the supported languages");
//        });
//    },
	
	insertResult: function(res){
		if(LappSpeech.langSwitch == "pl-PL")
			$('#insert_pl').val(res);
		else
			$('#insert').val(res);
	}
	
}