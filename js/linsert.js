var linsert = {
	
	packagesLength: null,
	rec:null,
	pac:null,
	
	initInsert: function(){
		var self = this;
		$(document).on('click','#insert-record',function(){
			self.insertRecord();
		});
		$(document).on('click','#add-package-button',function(){
			self.addPackage();
		});
		$(document).on('click','.del-unit',function(){
			var cid = $(this).attr('data-cid');
			self.rec.deleteUnit(cid,function(){
				self.showList();
			});
		});
		$(document).on('click','.del-package',function(){
			var cid = $(this).attr('data-cid');
			
			self.pac.deleteUnit(cid,function(){
				self.rec.updateConditional('package',cid,'package','0');
				self.packagesList();
			});
		});
		$(document).on('click','#del-all',function(){
			localStorage.clear();
		});
		
		// db init
		self.pac = new Empers({table:'packages',fields:['name','lang']});
		self.rec = new Empers({table:'units',fields:['native','foreign','package']});
		
		// select wyboru języka w dodawaniu paczki
		$('#message-details').on('click','#move-to-folder',function(e){
			e.preventDefault();
			setTimeout(function(){
				self.runThis();
			},100);
		});
	},
	
	showDropdown: function (element) {
		var event;
		event = document.createEvent('MouseEvents');
		event.initMouseEvent('mousedown', true, true, window);
		element.dispatchEvent(event);
	},
	
	insertSelectOptions: function(){
		var self = this;
		var selectPac = self.pac.getValues('name');
		setTimeout(function(){
			$.each(selectPac,function(k,v){
			var s = '<option value="'+v.key+'">'+v.value+'</option>';
				$('#select-package').append(s);
			});
		},100);
	},
	
	runThis: function (inp) { 
		var dropdown = document.getElementById('choose-lang');
		self.showDropdown(dropdown);
	},
	
	addPackage: function(){
		var self = this;
		var name = $.trim($('#package-name').val());
		var lang = $('#choose-lang option:selected').val();
		if(name == ""){
			alert('Wpisz nazwę!');
			return false;
		}
		self.pac.insertRecord([name,lang]);
		self.goToPage('packages-page');
	},
	
	countRecords: function(){
		var self = this;
		self.rec.all(function(i){
			self.dbLength = i.length;
		});
			
	},
	
	packagesList: function(){
		var self = this;
		var all = self.pac.getAll();
		var t = $('#package-list').empty();
		var first = '<li><span>Nieprzyporządkowane</span><a href="#list-unit-page/0">lista</a><a href="#exercise-page/0">ćwiczenia</a></li>';
		t.append(first);
		$.each(all,function(k,v){
			var li = '<li><span>'+v.name+'</span><span>'+v.lang+'</span><a href="#list-unit-page/'+v.cid+'">lista</a><a href="#exercise-page/'+v.cid+'">ćwiczenia</a><a class="_prevent del-package" data-cid="'+v.cid+'" href="#">usuń</a></li>';
			t.append(li);
		});
	},
	
	countPackages: function(){
		var self = this;
		self.rec.all(function(i){
			var c = 0;
			$.each(i,function(k,v){
				if(v.type == 'package'){
					c++;
				}
			});
			self.packagesLength = c;
		});
			
	},
	
	insertEditUnit: function(){
		if(this.param !== null){
			var el = this.rec.getAll('cid',this.param)[0];
			$('#insert').val(el.foreign);
			$('#insert_pl').val(el.native);
			setTimeout(function(){
				$('#select-package option[value='+el.package+']').prop('selected','selected');
			},200);
		}
	},
	
	insertRecord: function(){
		var self = this;
		var foreign = $.trim($('#insert').val());
		var polish = $.trim($('#insert_pl').val());
		var pac = $('#select-package option:selected').val();
		if(foreign == "" || polish == ""){
			alert('Wypełnij pola!');
			return false;
		}
		if(this.param === null){
			self.rec.insertRecord([polish,foreign,pac]);
			this.clearText();
		}else{
			// edycja
			self.rec.editRecord([polish,foreign,pac],self.param);
		}
	},
	
	showList: function(){
		var self = this;
		var all = self.rec.getAll('package',this.param);
		var t = $('#unit-list').empty();
		$.each(all,function(k,v){
			var li = '<li><span>'+v.native+' : '+v.foreign+'</span><a href="#insert-page/'+v.cid+'">edytuj</a><a href="#" class="_prevent del-unit" data-cid="'+v.cid+'">usuń</a></li>';
			t.append(li);
		});
	},
	
	clearText: function(){
		$('#insert').val("");
		$('#insert_pl').val("");
	}
	
};