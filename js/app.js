var phoneGapDet = (document.location.protocol == "file:");
$(function(){
	var app = new Lapp;
	if(!phoneGapDet)
		app.init();
	else
		document.addEventListener("deviceready", app.init, true);
});

var Lapp = function(){
	var self = this;
	
	this.dbLength = null;
	this.flashMsg = null;
	this.preventClick = false;
	this.startPage = 'packages-page';
	this.viewportWidth = $('.page-wrapper').width();
	this.pageSlide = false;
	this.hash;
//	this.mobile;

	this.init = function(){
		this.phonegap = phoneGapDet;
		
		if(this.phonegap){
			this.startEvent = 'touchstart';
			this.endEvent = 'touchend';
		}else{
			this.startEvent = 'mousedown';
			this.endEvent = 'mouseup';
		}
//		this.mobile = this.detectMobile();
//		alert(this.phonegap);
		// moduł wstawiania jednostek do bazy
		$.extend(self,linsert);
		self.initInsert();
		
//		$.extend(self,fiButton);
//		self.initFiButton();

		// moduł ćwiczeń
		// todo - może inicjalizacja dopiero po kliknięciu buttona ćwiczeń ?
		$.extend(self,lexerc);
		self.initExercises();
		
		$.extend(self,LappSpeech);
		if(this.phonegap)
			self.initSpeech();
		
//		$(document).on('click','a',function(e){
////			e.preventDefault();
//			self.additionnalAction($(e.target));
////			var href = $(this).attr('href').replace(/^#/, '');
////			self.changeHash(href);
//		});
		
		$(document).on('click','.fijs-button',function(){
			var el = $(this);
			el.addClass('fijs-touched');
			setTimeout(function(){
				el.removeClass('fijs-touched');
			},300);
		});
		
		self.onHashChange('start');
		
		window.addEventListener("hashchange", self.onHashChange, false);
		
//		if(this.phonegap)
//			document.addEventListener("backbutton", self.onBackKeyDown, false);
		
//		$('a[href=#packages-page]').click();
	}
	
//	this.changeHash = function(hash){
//		window.location.hash = '#'+hash;
//		this.hash = hash;
//		// rozpoznanie parametru
//		var p = hash.split('/');
//		if(p.length == 1){
//			this.param = null;
//			this.param2 = undefined;
//		}
//		else if(p.length == 2){
//			this.param = p[1];
//		}
//		else if(p.length === 3){	
//			this.param = p[2];
//			this.param2 = p[1];
//		}
//		this.switchPage(false);
//	};
	
	// alternatywa
	this.onHashChange = function(start){
		// start - po przeładowaniu lub otwarciu aplikacji
		if(start == 'start'){
			window.location.hash = '#'+self.startPage;
		}
		// active state na bieżącym linku anchor
		var hash = window.location.hash.replace(/^#/, '');
		self.hash = hash;
		self.setActiveAnchor();
		var p = hash.split('/');
		if(p.length == 1){
			self.param = null;
			self.param2 = undefined;
		}
		else if(p.length == 2){
			self.param = p[1];
		}
		else if(p.length === 3){	
			self.param = p[2];
			self.param2 = p[1];
		}
		self.switchPage(false);
	}
	
	this.switchPage = function(slide){
		var tmpl;
		if(this.hash == ''){
			tmpl = $('#'+this.startPage+'-Tmpl').tmpl();
		}else{
			tmpl = $('#'+this.hash+'-Tmpl').tmpl();
		}
		
		$('.page-wrapper .content-wrapper').empty().append(tmpl);
		
		// slide page
		if(slide){
			$('.page-wrapper  .content-wrapper').css({'margin-left':'-'+this.viewportWidth+'px'});
			$('.page-wrapper  .content-wrapper').animate({
				marginLeft:'0'
			},300);
		}
		
		// sprawdzenie wiadomosci flash
		if(this.flashMsg !== null){
			this.showFlash(this.flashMsg);
			this.flashMsg = null;
		}
		/**
         * @todo rozpoznawanie czy strona zosta?a prze?adowana - je?eli nie blokowac ponown? inicjalizacj? iscrolla [?]
         * lub destroy [?]
         */
		this.currentPage = $('.current-page .wrapper')[0];
		
		this.dispatch();
	};
	
	this.setActiveAnchor = function(){
		var a = $('a[href=#'+this.hash+']');
		$('.fijs-menu-button').removeClass('fijs-menu-active');
		a.addClass('fijs-menu-active');
		
		if(this.hash == 'packages-page')
			$('#addit-button').show();
		else
			$('#addit-button').hide();
	};
	
//	this.additionnalAction = function(el){
//		if(el.hasClass('fijs-menu-button')){
//			$('.fijs-menu-button').removeClass('fijs-menu-active');
//			el.addClass('fijs-menu-active');
//		}
//	};
	
	this.onBackKeyDown = function(){
		window.history.back();
		this.changeHash(window.location.hash.replace(/^#/, ''));
	}
	
	/**
     * Przechodzi do strony o podanym hashu
     */
	this.goToPage = function(hash,callback){
		window.location.hash = '#'+hash;
		this.onHashChange();
		if($.isFunction(callback)){
			callback.call(this);
		};
	};
	
	this.dispatch = function(){
		switch(this.hash){
			case "exercise-page":
				this.startExercises();
				break;
			case "list-unit-page":
				this.showList();
				break;
			case "packages-page":
				this.packagesList()
				break;
			case "insert-page":
				this.insertEditUnit();
				this.insertSelectOptions();
//				if(!phoneGapDet){
//					$('.recognize-button').hide();
//				}
				break;
			case "test":
				
				break;
			case "list-unit-page":
				break;
		}
	};
	
	this.detectMobile = function(){
		if(this.phonegap){
			return true;
		}else{
			return navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/);
		}
	};	
}