var fijsTool = {
	
	/**
	 * True if mobile
	 */
	mobileDevice: false,
	
	/**
	 * Click/touch event trigger (mobile/fix)
	 */
	triggerEvent:null,
	
	/**
	 * Global settings
	 */
	initFijsTool: function(){
		// mobile detect
		if(this.detectMobile()){
			this.startEvent = 'touchstart';
			this.endEvent = 'touchend';
		}else{
			this.startEvent = 'mousedown';
			this.endEvent = 'mouseup';
		}
	},
	
	/**
	 * Returns true if mobile browser
	 */
	detectMobile: function(){
		var check = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/);
		return check !== null || this.phonegap ? true: false;
	}	
	
}