var Empers = function(obj){
	var self = this;
	this.tableName = obj.table;
	this.fields = obj.fields;
	// dodanie pola cid
	this.fields.push('cid');
	this.db;
	this.count;
	
	var i = localStorage.getItem(self.tableName);
	if(i === null){
		this.db = localStorage.setItem(self.tableName,JSON.stringify([]));
	}else{
		this.db = JSON.parse(i);
	}
	
	this.insertRecord = function(arr,key){
		self.countRecords();
		// @todo walidacja pól (czy są wymagane, potem może typ danych)
		var el = {};
		for(var i in arr){
			el[self.fields[i]] = arr[i];
		}
		el['cid'] = this.count + 1;
		this.db.push(el);
		self.countRecords();
		self.persistTable();
	};
	
	/**
	 * Przepisuje obiekt - wpisuje wszystkie nowe wartości
	 */
	this.editRecord = function(arr,key){
		arr.push(key);
		var el = this.getItemByCid(key);
		for(var i=0;i<=arr.length;i++){
			el[self.fields[i]] = arr[i];
		}
		var index = this.getIndexByCid(key);
		this.db[index] = el;
		this.persistTable();
	};
	
	/**
	 * Edytuje dane pole obiektu
	 */
	this.editField = function(cid,field,value){
		var index = this.getIndexByCid(cid);
		this.db[index][field] = value;
		this.persistTable();
	};
	
	this.updateConditional = function(whereField,whereValue,targetField,newValue){
		var all = this.getAll(whereField,whereValue);
		for(var i in all){
			var cid = all[i]['cid'];
			var index = self.getIndexByCid(cid);
//			console.log(this.db[index][targetField]);
//			console.log(newValue);
			this.db[index][targetField]= newValue;
		}
		this.persistTable();
	};
	
	this.getIndexByCid = function(cid){
		var out;
		for(var i in this.db){
			if(this.db[i]['cid'] == cid)
				out = i;
		}
		return out;
	};
	
	this.persistTable = function(){
		localStorage.removeItem(self.tableName);
		localStorage.setItem(self.tableName,JSON.stringify(self.db));
	};
	
	/**
	 * Zwraca jednostkę wg klucza
	 * na razie nie używana
	 */
	this.getItemByCid = function(cid){
		return this.getAll('cid',cid)[0];
	};
	
	/**
	 * Zwraca indeks kolekcji na podstawie pola cid
	 */
	this.getIdByCid = function(cid){
		var out = null;
		for(var i in this.db){
			if(this.db[i]['cid'] == cid)
				out = i;
		}
		if(out !== null){
			return out;
		}else{
			return 'error';
		}
	};
	
	/**
	 * Wydobywa wszystkie dane danej tabeli
	 */
	this.getAll = function(field,value){
		var out = [];
		for(var i in this.db){
			if(field !== undefined && value !== undefined){
				if(this.db[i][field] == value)
					out.push(this.db[i]);
			}else{
				out.push(this.db[i]);
			}
		}
		return out;
	};
	
	/**
	 * wydobywa wszystkie wartości, wg. zadanego klucza
	 */
	this.getValues = function(key){
		var out = [];
		for(var i in this.db){
			var obj = {};
			obj.value = this.db[i][key];
			obj.key = this.db[i]['cid'];
			out.push(obj);
		}
		return out;
	};
	
	this.deleteUnit = function(cid,callback){
		var index = this.getIdByCid(cid);
		this.db.splice(index,1);
		this.persistTable();
		if(typeof callback == 'function'){
			callback.call(this);
		}
	};
	
	this.countRecords = function(){
		var c = 0;
		for(var i in this.db){
			c++;
		}
		self.count = c;
	};
	this.countRecords();
}