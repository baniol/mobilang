var lexerc = {
	currentKey:null,
	currentFor:null,
	// tablica (paczka) z kluczami jednostek
	currentPackage:[],
	
	initExercises: function(){
		var self = this;
		$(document).on('click','#dalej',function(){
			self.nextUnit();
		});
		$(document).on('click','#sprawdz',function(){
			self.checkSentence();
		});
		$(document).on('click','#umiem',function(){
			self.markUnit();
		});
	},
	
	/*
	 * Inicjalizuje 'paczkę' ćwiczeń
	 */
	startExercises: function(){
		var self = this;
		var all = this.rec.getAll('package',this.param);
		$.each(all,function(k,v){
			self.currentPackage.push(v.cid);
		});
		self.nextUnit();
	},
	
	nextUnit: function(){
		var self = this;
		var rand = this.getRandom();
		var key = parseInt(self.currentPackage[rand]);
		if(key == this.currentKey){
			self.nextUnit();
			return false;
		}
		this.currentKey = key;
		if(self.currentPackage.length > 0){
			var obj = self.rec.getItemByCid(key);
			$('#pol-sentence').text(obj.native);
			$('#for-sentence').text('');
			self.currentFor = obj.foreign;
		}else{
			$('#pol-sentence').text('KONIEC');
		}
	},
	
	getRandom: function(){
		var exLength = this.currentPackage.length;
		return Math.floor((Math.random()*exLength));
	},
	
	/*
	 * Oznacza jednostkę jako nauczoną
	 */
	markUnit: function(){
		var self = this;
		for(var i=0;i<=self.currentPackage.length;i++){
			if(self.currentKey == self.currentPackage[i]){
				self.currentPackage.splice(i,1);
			}
		}
		self.nextUnit();
	},
	
	checkSentence: function(){
		$('#for-sentence').text(this.currentFor);
//		self.rec.nuke();
	}
}
